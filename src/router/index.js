import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Dashboard from '@/components/Dashboard'

import BookList from '@/components/role/list'
import UserChangePwd from '@/components/user/changepwd'
import UserProfile from '@/components/user/profile'

import MenuList from '@/components/menu/list'

import FileList from '../components/file/List.vue'

import LogList from '@/components/log/List'
import CallTaskList from '@/components/CallTask/CallTaskList'
import  AddTask from '@/components/CallTask/AddTask'
import CallNoteList from '@/components/CallTask/CallNoteList'
import CallDetail from '@/components/CallTask/CallDetail'
import CallProcessList from '@/components/CallProcess/CallProcessList'
import ContactPersonList from '@/components/ContactPerson/ContactPersonList'
import addContactPersonGroup from '@/components/ContactPerson/addContactPersonGroup'
import ContactBlackList from '@/components/ContactPerson/ContactBlackList'
import DataAnalysis from '@/components/DataAnalysis/DataAnalysis'

// 懒加载方式，当路由被访问的时候才加载对应组件
const Login = resolve => require(['@/components/Login'], resolve)

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path:'/login',
      component:Login
    },
    {
      path: '/',
      name: 'home',
      component: Home,
     // redirect: '/dashboard',

      leaf: true, // 只有一个节点
      menuShow: true,
      iconCls: 'fa fa-home', // 图标样式class
      children: [
        {path: '/dashboard', component: Dashboard, name: '首页', menuShow: true}
      ]
    },
    {
      path: '/',
      component: Home,
      name: 'AI外呼',
      menuShow: true,
      leaf: true, // 只有一个节点
      iconCls: 'fa fa-volume-control-phone', // 图标样式class
      children: [
        {path: '2-1', component: CallTaskList, name: 'CallTaskList', menuShow: true},
        {path: 'addTask', component: AddTask,name:'addTask', menuShow: true},
        {path: '2-2', component: CallNoteList, name: 'CallNoteList', menuShow: true},
        {path:'callDetail/:taskId?',component:CallDetail,name:'CallDetail'}
      ]
    },
    {
      path: '/',
      component: Home,
      name: 'AI外呼流程',
      menuShow: true,
      leaf: true, // 只有一个节点
      iconCls: 'fa fa-code-fork', // 图标样式class
      children: [
        {path: '3-1', component: CallProcessList, name: 'CallProcessList', menuShow: true},
      ]
    },
    {
      path: '/',
      component: Home,
      name: '联系人组',
      menuShow: true,
      leaf: true, // 只有一个节点
      iconCls: 'fa fa-address-book', // 图标样式class
      children: [
        {path: '4-1', component: ContactPersonList, name: 'ContactPersonList', menuShow: true},
        {path:'addContactPersonGroup',component:addContactPersonGroup,name:'addContactPersonGroup'},
        {path: '4-2', component: ContactBlackList, name: 'ContactBlackList', menuShow: true},
      ]
    },
    {
      path: '/',
      component: Home,
      name: '监控报表',
      menuShow: true,
      leaf: true, // 只有一个节点
      iconCls: 'fa fa-bar-chart', // 图标样式class
      children: [
        {path: '5', component: DataAnalysis, name: 'DataAnalysis', menuShow: true}
      ]
    },
    {
      path: '/',
      component: Home,
      name: '菜单管理',
      menuShow: true,
      leaf: true, // 只有一个节点
      iconCls: 'fa fa-server', // 图标样式class
      children: [
        {path: '/admin/menu', component: MenuList, name: '菜单列表', menuShow: true}
      ]
    },
    {
      path: '/',
      component: Home,
      name: '角色管理',
      menuShow: true,
      leaf: true,
      iconCls: 'fa fa-group',
      children: [
        {path: '/admin/role', component: BookList, name: '角色管理', menuShow: true},
      ]
    },

    {
      path: '/',
      component: Home,
      name: '文件管理',
      menuShow: true,
      leaf: true,
      iconCls: 'fa fa-group',
      children: [
        {path: '/cms/file', component: FileList, name: '文件管理', menuShow: true},
      ]
    },
    {
      path: '/',
      component: Home,
      name: '日志管理',
      menuShow: true,
      leaf: true,
      iconCls: 'fa fa-group',
      children: [
        {path: '/base/log', component: LogList, name: '日志管理', menuShow: true},
      ]
    },
    {
      path: '/',
      component: Home,
      name: '设置',
      menuShow: true,
      iconCls: 'iconfont icon-setting1',
      children: [
        {path: '/user/profile', component: UserProfile, name: '个人信息', menuShow: true},
        {path: '/user/changepwd', component: UserChangePwd, name: '修改密码', menuShow: true}
      ]
    },

    {
      path: '/',
      component: Home,
      name: '注册服务',
      menuShow: true,
      iconCls: 'iconfont icon-setting1',
      children: [
        {path: '/service',component: UserProfile,  redirect: 'http://baidu.com',target:'_blank',name: '个人信息', menuShow: true},
      ]
    },
  ]
})

/*router.beforeEach((to, from, next) => {
  // console.log('to:' + to.path)
  if (to.path.startsWith('/login')) {
    window.localStorage.removeItem('access-token')
    //window.localStorage.removeItem('access-user')
    next()
  } else if (to.path.startsWith('/dashboard')) {
    next()
  } else {
    //let user = JSON.parse(window.localStorage.getItem('access-token'))
    let user = window.localStorage.getItem('access-token');
    if (!user) {
      next({path: '/login'})
    } else {
      next()
    }
  }
})*/

export default router
