# vueBack

#### 项目介绍
基于vue+elementUI的后台管理系统模板

#### 软件架构
vue+element webpack node 可用作前后端分离的前台框架

![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/104602_597553fc_1356068.png "屏幕截图.png")
#### 相关技术
vuejs2.0：一套构建用户界面的渐进式框架，易用、灵活、高效。
element-ui：一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的组件库。
vue-router：官方的路由组件，配合vue.js创建单页应用（SPA）非常简单。
axios: 基于 Promise 的 HTTP 请求客户端，可同时在浏览器和 node.js 中使用。

#### 安装教程

![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/104706_8e1931a0_1356068.png "屏幕截图.png")


#### 系统截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/104936_a0ae43e6_1356068.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/105022_c19f3424_1356068.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/105101_41de4ca4_1356068.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/105123_69001c67_1356068.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/105143_7332d1dc_1356068.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0921/105207_565838cd_1356068.png "屏幕截图.png")
